#ifndef FIVE_IN_A_ROW_BOARD_WALKER_H
#define FIVE_IN_A_ROW_BOARD_WALKER_H

#include "Board.h"

#include <vector>

namespace walk
{

bool inBounds( const Location& loc );

Location next( const Location& current, Direction direction );

std::vector< Location > boardRange( const Location& origin, Direction direction, int length );

} // namespace walk

#endif // header include guard
