#ifndef FIVE_IN_A_ROW_BOARD_PLAYER
#define FIVE_IN_A_ROW_BOARD_PLAYER

#include "Board.h"
#include "BoardWalker.h"
#include "Move.h"
#include "Token.h"

#include <memory>
#include <vector>

class Player
{
 public:
  Player( Token myTokenType )
    : mTokenType( myTokenType )
  {}
  std::vector< std::unique_ptr< Move > > choices( const Board& board ) const;

  bool hasWon( const Board& board, const Move& lastPlayed ) const;
  
 private:
  Token mTokenType;
};
#endif // header include guard
