#include "BoardWalker.h"

#include <stdexcept>
#include <utility>
#include <vector>

namespace walk
{

bool inBounds( const Location& loc )
{
  return loc.row() >= 0 && loc.col() >= 0 && loc.row() < DIMENSION && loc.col() < DIMENSION;
}

Location next( const Location& current, Direction direction )
{
  if ( direction == Direction::Horizontal ) {
    return Location( current.row(), current.col() + 1 );
  } else if ( direction == Direction::Vertical ) {
    return Location( current.row() + 1, current.col() );
  } else if ( direction == Direction::DownRight ) {
    return Location( current.row() + 1, current.col() + 1 );
  } else { // Direction::UpRight
    return Location( current.row() - 1, current.col() + 1 );
  }
}

std::vector< Location > boardRange( const Location& origin, Direction direction, int length )
{
  std::vector< Location > locations;
  if ( length < 1 ) {
    throw std::logic_error("Range requested with non-positive length");
  }
  if ( !inBounds( origin ) ) {
    throw std::runtime_error("Bad Location for Walker origin");
  }
  const int inclusiveSpan = length - 1;
  const int oRow = origin.row();
  const int oCol = origin.col();
  Location start(0,0), end(0,0);
  if ( direction == Direction::Vertical ) {
    start =  Location( oRow - inclusiveSpan, oCol );
    end = Location( oRow + length, oCol );
  } else if ( direction == Direction::DownRight ) {
    start =  Location( oRow - inclusiveSpan, oCol - inclusiveSpan );
    end = Location( oRow + length, oCol + length );
  } else if ( direction == Direction::Horizontal ) {
    start =  Location( oRow, oCol - inclusiveSpan );
    end = Location( oRow, oCol + length );
  } else { // UpRight
    start =  Location( oRow + inclusiveSpan, oCol - inclusiveSpan );
    end = Location( oRow - length, oCol + length );
  }
  Location iter = start;
  while ( !inBounds( iter ) ) {
    iter = next( iter, direction );
  }
  while ( inBounds( iter ) && iter != end ) {
    locations.push_back( iter );
    iter = next( iter, direction );
  }
  return locations;
}

} // namespace walk
