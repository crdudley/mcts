#include "Player.h"

#include "Board.h"
#include "BoardWalker.h"
#include "Display.h"

#include <algorithm>
#include <set>

namespace {

bool foundFiveInARow( const Board& board, const Token token, const std::vector< Location >& range)
{
  if ( range.size() < 5 ) {
    return false;
  }
  std::vector< Token > rangeTokens( range.size() );
  std::transform(
    range.begin(),
    range.end(),
    rangeTokens.begin(),
    [&board](Location loc){ return board[loc];} );
  
  std::vector< Token >::iterator firstOfFive, lastOfFive;
  for ( firstOfFive = rangeTokens.begin(), lastOfFive = rangeTokens.begin()+4;
        lastOfFive != rangeTokens.end();
        firstOfFive++, lastOfFive++ )
  {
    if ( std::count( firstOfFive,lastOfFive+1, token ) > 4 ) {
      return true;
    }
  }
  return false;
}

} // anon namespace

bool Player::hasWon( const Board& board, const Move& lastPlayed ) const
{
  Location current = lastPlayed.location();
  bool won = foundFiveInARow( board, mTokenType, walk::boardRange( current, Direction::UpRight, 5 ) )
    || foundFiveInARow( board, mTokenType, walk::boardRange( current, Direction::DownRight, 5 ) )
    || foundFiveInARow( board, mTokenType, walk::boardRange( current, Direction::Vertical, 5  ) )
    || foundFiveInARow( board, mTokenType, walk::boardRange( current, Direction::Horizontal, 5 ) );
  if ( won ) {
    bool debug = false;
    if ( debug ) {
      display::display( board );
    }
  }
  return won;
}

std::vector< std::unique_ptr< Move > > Player::choices( const Board& board ) const
{
  const int initialTurns = 15;
  const int closeRangeUser = 3;
  const int closeRangeComputer = 2;
  std::vector< std::unique_ptr< Move > > result;
  long int turn = std::count( board.allTokens().begin(), board.allTokens().end(), Token::User );
  auto isBlank = [&board]( Location l ){ return board[l] == Token::Blank; };
  if ( turn < initialTurns ) {
    std::vector< Location > choiceLocations;
    choiceLocations.reserve( 200 );
    for ( int i = 0; i < board.dimension(); i++ ) {
      for ( int j = 0; j < board.dimension(); j++ ) {
        Token currentToken = board[Location(i,j)];
        if ( !isBlank( {i,j} ) ) {
          auto upRightRange = currentToken == Token::User ?
            walk::boardRange( {i,j}, Direction::UpRight, closeRangeUser ) :
            walk::boardRange( {i,j}, Direction::UpRight, closeRangeComputer );
          auto downRightRange = currentToken == Token::User ?
            walk::boardRange( {i,j}, Direction::DownRight, closeRangeUser ) :
            walk::boardRange( {i,j}, Direction::DownRight, closeRangeComputer );
          auto verticalRange = currentToken == Token::User ?
            walk::boardRange( {i,j}, Direction::Vertical, closeRangeUser ) :
            walk::boardRange( {i,j}, Direction::Vertical, closeRangeComputer );
          auto horizontalRange = currentToken == Token::User ?
            walk::boardRange( {i,j}, Direction::Horizontal, closeRangeUser ) :
            walk::boardRange( {i,j}, Direction::Horizontal, closeRangeComputer );
          std::copy_if( upRightRange.begin(),
            upRightRange.end(),
            std::back_inserter( choiceLocations ),
            isBlank );
          std::copy_if( downRightRange.begin(),
            downRightRange.end(),
            std::back_inserter( choiceLocations ),
            isBlank );
          std::copy_if( verticalRange.begin(),
            verticalRange.end(),
            std::back_inserter( choiceLocations ),
            isBlank );
          std::copy_if( horizontalRange.begin(),
            horizontalRange.end(),
            std::back_inserter( choiceLocations ),
            isBlank );
          std::sort( choiceLocations.begin(), choiceLocations.end() );
          auto newEnd = std::unique( choiceLocations.begin(), choiceLocations.end() );
          // No default Location ctor so cannot use resize()
          while ( choiceLocations.end() != newEnd ) { choiceLocations.pop_back(); }
        }
      }
    }
    for ( const auto& loc : choiceLocations ) {
      result.emplace_back( std::make_unique< Move >( loc, mTokenType ) );
    }
  } else {
    for ( int i = 0; i < board.dimension(); i++ ) {
      for ( int j = 0; j < board.dimension(); j++ ) {
        if ( board[Location(i,j)] == Token::Blank ) {
          result.emplace_back( std::make_unique< Move >( Location(i,j), mTokenType ) );
        }
      }
    }
  }
  return result;
}
