#include "MCTS.h"

#include "Board.h"
#include "Display.h"
#include "Move.h"
#include "Player.h"
#include "Timer.h"

#include <cstdlib>
#include <ctime>
#include <iostream>

Location promptUser( const Board& board )
{
  int rowPlay, colPlay;
  display::display( board );
  std::cout << "Pick your spot, row number followed by column number: ";
  std::cin >> rowPlay >> colPlay;
  rowPlay--; colPlay--;
  bool found = false;
  while ( !found )
  {
    if ( board[Location( rowPlay, colPlay )] == Token::Blank )
    {
      found = true;
    }
    else
    {
      std::cout << "That spot is already taken. Try again: ";
      std::cin >> rowPlay >> colPlay;
      rowPlay--; colPlay--;
    }
  }
  return Location( rowPlay, colPlay );
}

int main()
{
  Board displayBoard;
  Timer timer( 2500 );
  Player user( Token::User );
  Player computer( Token::Ai );
  bool ended = false;

  Move initialUserPlay = Move( promptUser( displayBoard ), Token::User );
  MonteCarloTreeSearch<Move,Board,Player,Timer> decider( displayBoard, user, computer, timer );
  displayBoard = initialUserPlay.forward( displayBoard );
  Move aiMove = decider.play( initialUserPlay );
  std::cout << "Computer plays " << aiMove.location().row() + 1 << ", " << aiMove.location().col() + 1 << std::endl;
  displayBoard = aiMove.forward( displayBoard );
  while ( !ended ) {
    Move userPlay = Move( promptUser( displayBoard ), Token::User );
    displayBoard = userPlay.forward( displayBoard );
    if ( user.hasWon( displayBoard, userPlay ) ) {
      display::display( displayBoard );
      std::cout << "Congratulations! You won!" << std::endl;
      ended = true;
    }
    else {
      aiMove = decider.play( userPlay );
      std::cout << "Computer plays " << aiMove.location().row() + 1 << ", " << aiMove.location().col() + 1 << std::endl;
      displayBoard = aiMove.forward( displayBoard );
      if ( computer.hasWon( displayBoard, aiMove ) ) {
        display::display( displayBoard );
        std::cout << "Better luck next time" << std::endl;
        ended = true;
      }
    }
  }
  
  return 0;
}
