#include "Move.h"

Board Move::forward( const Board& board ) const
{
  if ( board[mLocation] != Token::Blank ) {
    throw std::logic_error( "Trying to replace non-blank token" );
  }
  Board result( board );
  result[mLocation] = mToken;
  return result;
}

Board Move::reverse( const Board& board ) const
{
  if ( board[mLocation] == Token::Blank ) {
    throw std::logic_error( "Trying to revert currently blank token" );
  }
  Board result( board );
  result[mLocation] = Token::Blank;
  return result;
}

bool Move::operator==( const Move& other ) const
{
  return mLocation == other.mLocation && mToken == other.mToken;
}
