#ifndef FIVE_IN_A_ROW_BOARD_TIMER
#define FIVE_IN_A_ROW_BOARD_TIMER

class Timer
{
 public:
 Timer( int maxDecisions )
   : mMaxDecisions( maxDecisions )
  {}
  void start() { mCurrentDecisionCount = 0; }
  bool ended() { return ++mCurrentDecisionCount > mMaxDecisions; }
 private:
  int mCurrentDecisionCount = 0;
  int mMaxDecisions;
};
#endif // header include guard
