#include "Board.h"
#include "Location.h"

Board::Board()
{
  std::fill( mTokens.begin(), mTokens.end(), Token::Blank );
}

Token& Board::operator[]( const Location& idx)
{
  return mTokens[idx.row()*DIMENSION+idx.col()];
}

const Token& Board::operator[]( const Location& idx) const
{
  return mTokens[idx.row()*DIMENSION+idx.col()];
}

