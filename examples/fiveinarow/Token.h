#ifndef FIVE_IN_A_ROW_TOKEN_H
#define FIVE_IN_A_ROW_TOKEN_H

enum class Token
{
  User,
  Ai,
  Blank
};

#endif
