#ifndef FIVE_IN_A_ROW_DISPLAY
#define FIVE_IN_A_ROW_DISPLAY

#include <sstream>

class Board;

namespace display
{
void display( const Board& board );
// for testing purposes
void display( const Board& board, std::stringstream& buffer );
}
#endif // header include guard
