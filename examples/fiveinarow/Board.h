#ifndef FIVE_IN_A_ROW_BOARD_H
#define FIVE_IN_A_ROW_BOARD_H

#include "Location.h"
#include "Token.h"

#include <array>

constexpr int DIMENSION  = 10;

enum class Direction {Horizontal, DownRight, Vertical, UpRight};

// Board contains an array of tokens stored row major.
class Board
{
public:
  Board();
  Token tokenAt( const Location& location ) const { return (*this)[location]; }
  int dimension() const { return DIMENSION; }
  Token& operator[]( const Location& idx );
  const Token& operator[]( const Location& idx ) const;
  const std::array< Token, DIMENSION*DIMENSION >& allTokens() const { return mTokens; }
private:
  std::array< Token, DIMENSION*DIMENSION > mTokens;
};

#endif // header include guard
