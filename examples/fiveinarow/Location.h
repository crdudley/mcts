#ifndef FIVE_IN_A_ROW_LOCATION
#define FIVE_IN_A_ROW_LOCATION

#include <utility>

class Location
{
public:
  Location( const int row, const int col )
    : mRow( row ), mCol( col )
  {}

  Location(const Location& other)
    : mRow( other.row() ), mCol( other.col() )
  {}

  int row() const { return mRow; }
  int col() const { return mCol; }

  bool operator<( const Location& other ) const
  {
    return mRow == other.mRow ? mCol < other.mCol : mRow < other.mRow;
  }
  
  bool operator==( const Location& other ) const
  {
    return ( mRow == other.row() && mCol == other.col() );
  }
  bool operator!=( const Location& other ) const
  {
    return !( *this == other );
  }
  Location& operator=( const Location& other )
  {
    mRow = other.row();
    mCol = other.col();
    return *this;
  }

private:
  int mRow;
  int mCol;
};

#endif // header include guard
