#ifndef FIVE_IN_A_ROW_MOVE_H
#define FIVE_IN_A_ROW_MOVE_H

#include "Board.h"
#include "Location.h"
#include "Token.h"

class Move
{
 public:
  Move( Location location, Token token )
    : mLocation( location ),
    mToken( token )
  {}
  // Functions required by MCTS
  Board forward( const Board& board ) const;
  Board reverse( const Board& board ) const;
  bool operator==( const Move& other ) const;

  // Functions required by FiveInARow game
  Location location() const { return mLocation; }
 private:
  Location mLocation;
  Token mToken;
};

#endif
