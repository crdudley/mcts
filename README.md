# README #

This is a header only library written in C++11 implementing the Monte Carlo Tree Search method for A.I. game logic.
You do not need to inherit from anything in this library to utilize it, rather your classes must implement certain functions,
which the library will call in the process of it's tree search.
