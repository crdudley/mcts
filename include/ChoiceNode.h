#ifndef MCTS_CHOICE_NODE_H
#define MCTS_CHOICE_NODE_H

#include "ExistenceChecks.h"

#include <memory>
#include <vector>

template< typename Choice >
class ChoiceNode
{
 public:
 ChoiceNode(std::unique_ptr< Choice >&& choice, std::shared_ptr< ChoiceNode< Choice > > parent )
   : mMove(std::move(choice)),
   mParent(parent)
   {
     static_assert(can_compare<Choice>::value, "Must define equality operator for Choice class");
   }

  // if Choice is not one of my children, return nullptr
  std::shared_ptr< ChoiceNode< Choice > > findChild(const Choice& choice) const
  {
    for (std::shared_ptr< ChoiceNode< Choice > > child : mChildren) {
      if ( child->mMove && *(child->mMove) == choice) {
        return child;
      }
    }
    return nullptr;
  }

  // Need modifiable ChoiceNodes, but don't want to alter who the children
  // are from outside the class, so return a copy.
  std::vector< std::shared_ptr< ChoiceNode< Choice > > > children() const
  {
    return mChildren;
  }
  std::shared_ptr< ChoiceNode< Choice > > parent() const
  {
    return mParent;
  }
  Choice* myMove() const { return mMove.get(); }

  bool isFinished() const { return mFinished; }
  int wins() const { return mWinsFound; }
  int losses() const { return mLossesFound; }
  int totalSimsRun() const { return mWinsFound + mTiesFound + mLossesFound; }

  void setFinished() { mFinished = true; }
  void setWins( int numWins ) { mWinsFound = numWins; }
  void setLosses( int numLosses ) { mLossesFound = numLosses; }
  void setTies( int numTies ) { mTiesFound = numTies; }
  
  void addChild( std::shared_ptr< ChoiceNode< Choice > > newChild )
  {
    mChildren.emplace_back( newChild );
  }
  
 private:
  std::unique_ptr< Choice > mMove;
  std::shared_ptr< ChoiceNode< Choice > > mParent = nullptr;
  std::vector< std::shared_ptr< ChoiceNode< Choice > > > mChildren = {};
  int mLossesFound = 0;
  int mTiesFound = 0;
  int mWinsFound = 0;
  bool mFinished = false;
};

#endif
