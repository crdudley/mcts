#ifndef MCTS_EXISTENCE_CHECKS_H
#define MCTS_EXISTENCE_CHECKS_H

#include<memory>
#include<type_traits>
#include<vector>

// SFINAE tests
template <typename Choice, typename State>
  class has_forward
{
  template <typename C> static typename std::is_same< decltype(std::declval<C>().forward(std::declval<State>())), State>::type test(C*);
  template <typename C> static std::false_type test(...);

 public:
  static constexpr bool value = decltype(test<Choice>(0))::value;
};

// SFINAE test
template <typename Choice, typename State>
  class has_reverse
{
  template <typename C> static typename std::is_same< decltype(std::declval<C>().reverse(std::declval<State>())), State>::type test(C*);
  template <typename C> static std::false_type test(...);

 public:
  static constexpr bool value = decltype(test<Choice>(0))::value;
};

// SFINAE test
template <typename Choice, typename State, typename Analyzer>
  class has_choices
{
  template <typename C> 
    static typename std::is_convertible< typename decltype(std::declval<C>().choices(std::declval<State>()))::value_type, std::unique_ptr< Choice > >::type test(C*);
	
  template <typename C> 
    static std::false_type test(...);

 public:
  static constexpr bool value = decltype(test<Analyzer>(0))::value;
};

// SFINAE test
template <typename Timer>
class has_start
{
  template <typename C>
    static typename std::is_same< decltype(std::declval<C>().start()), void >::type test(C*);

  template <typename C>
    static std::false_type test(...);

 public:
  static constexpr bool value = decltype(test<Timer>(0))::value;
};

// SFINAE test
template <typename Timer>
class has_ended
{
  template <typename C>
    static typename std::is_same< decltype(std::declval<C>().ended()), bool >::type test(C*);

  template <typename C>
    static std::false_type test(...);

 public:
  static constexpr bool value = decltype(test<Timer>(0))::value;
};

// SFINAE test
template <typename Choice>
class can_compare
{
  template <typename C>
    static typename std::is_same< decltype(std::declval<C>() == std::declval<C>()), bool >::type test(C*);

  template <typename C>
    static std::false_type test(...);

 public:
  static constexpr bool value = decltype(test<Choice>(0))::value;
};

#endif
