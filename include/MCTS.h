#ifndef MCTS_MCTS_H
#define MCTS_MCTS_H

#include "ChoiceNode.h"
#include "ExistenceChecks.h"

#include <algorithm>
#include <iostream>
#include <random>

template< typename Choice, typename State, typename Analyzer, typename Timer >
class MonteCarloTreeSearch
{
  // Need some aliases
  using Node = ChoiceNode< Choice >;
  using NodePtr = std::shared_ptr< Node >;
  using OptionsIter = typename std::vector< NodePtr >::iterator;

public:
 MonteCarloTreeSearch( State& initialState, Analyzer& user, Analyzer& computer, Timer& timer )
   : mState( initialState ),
    mUser( user ),
    mComputer( computer ),
    mTimer( timer )
  {
    // Verify the analyzer class has the requisite functionality
    static_assert(has_choices<Choice, State, Analyzer>::value,
      "Analyzer class must have std::vector< std::unique_ptr< Choice > > choices(State) function");

    // Verify the timer class has the requisite functionality
    static_assert(has_start<Timer>::value, "Timer class must have void start() method");
    static_assert(has_ended<Timer>::value, "Analyzer class must have bool ended() method");

    // Verify the Choice class has the requisite functionality
    static_assert(has_forward<Choice, State>::value, "Choice class must have State forward(State) function");
    static_assert(has_reverse<Choice, State>::value, "Choice class must have State reverse(State) function");
    static_assert(can_compare<Choice>::value, "Must define equality operator for Choice class");

    std::mt19937 rng;
    mRandom.seed(std::random_device()());
  }

  Choice play(const Choice& userChoice )
  {
    mTimer.start();
    mState = userChoice.forward(mState);
    std::vector< NodePtr > potentialChoices;
    // The following variable is for total simulations run at this stage.
    // Presumably it's zero, but if we've already been down this path
    // before (i.e. if userChoiceNode is not null, we'll have some runs already.
    int totalSimsRun = 0;
    if (mLastPlayed) {
      NodePtr userChoiceNode = mLastPlayed->findChild(userChoice);
      if (userChoiceNode) {
        totalSimsRun = userChoiceNode->totalSimsRun();
        potentialChoices = userChoiceNode->children();
      }
    }
    if (potentialChoices.empty()) {
      std::vector< std::unique_ptr< Choice > > computerOptions = mComputer.choices(mState);
      for (auto&& option : computerOptions) {
        potentialChoices.emplace_back( std::make_shared<Node>(std::move(option), mLastPlayed));
      }
    }

    while (!mTimer.ended()) {
      bool userTurn = false;
      NodePtr child = chooseChildToSimulate( totalSimsRun, potentialChoices, userTurn );
      totalSimsRun++;
      simulateUserResponse( child );
    }

    // Computer choice is whichever node has the most runs. If there's a tie, the # of wins
    // constitutes the tie breaker. One caveat is that the end condition has to be looked for,
    // which is a finished node (likely with a low total but a high win percentage)
    auto totalComp =
      []( const NodePtr& n1, const NodePtr& n2 ){ return n1->totalSimsRun() < n2->totalSimsRun(); };
    auto winsComp = []( const NodePtr& n1, const NodePtr& n2 ){ return n1->wins() < n2->wins(); };
    std::sort( potentialChoices.begin(), potentialChoices.end(), totalComp );
    OptionsIter lower = std::lower_bound( potentialChoices.begin(),
      potentialChoices.end(),
      potentialChoices.back(),
      totalComp );
    auto bestPossibility = *std::max_element( lower, potentialChoices.end(), winsComp );
    // one more linear search through to find a finished node with a higher win %
    double winPercentage = bestPossibility->wins() / static_cast< double >( bestPossibility->totalSimsRun() );
    auto goodFinish = std::find_if( potentialChoices.begin(),
      potentialChoices.end(),
      [winPercentage]( const NodePtr& n ){ return n->isFinished()
        && ( n->wins() / static_cast< double >( n->totalSimsRun() ) >= winPercentage ); } );
    if ( goodFinish != potentialChoices.end() ) {
      bestPossibility = *goodFinish;
      winPercentage = bestPossibility->wins() / static_cast< double >( bestPossibility->totalSimsRun() );
    }
    std::cout << "Confidence level: " << winPercentage;
    std::cout << " (" << bestPossibility->totalSimsRun() << " runs)" << std::endl;
    mLastPlayed = bestPossibility;
    mState = bestPossibility->myMove()->forward( mState );
    return *bestPossibility->myMove();
  }

  void simulateUserResponse( NodePtr computerNode )
  {
    Choice* choice = computerNode->myMove();
    mState = choice->forward( mState );
    if ( isEndState( computerNode, false ) ) {
      return;
    }
    int totalSimsRun = computerNode->totalSimsRun();
    std::vector< NodePtr > potentialChoices = computerNode->children();
    if ( potentialChoices.empty() ) {
      std::vector< std::unique_ptr< Choice > > userOptions = mUser.choices( mState );
      for ( auto&& option : userOptions ) {
        potentialChoices.emplace_back( std::make_shared<Node>( std::move( option ), mLastPlayed ) );
      }
    }

    bool userTurn = true;
    NodePtr child = chooseChildToSimulate( totalSimsRun, potentialChoices, userTurn );
    deepDive( child, !userTurn );
    updateTotals( computerNode );
    mState = choice->reverse( mState );
  }

  void deepDive( NodePtr choiceNode, bool userTurn)
  {
    Choice* choice = choiceNode->myMove();
    if ( choice == nullptr ) {
      return;
    }
    mState = choice->forward( mState );
    if ( isEndState( choiceNode, userTurn ) ) {
      return;
    }
    std::vector< NodePtr > possibilities = choiceNode->children();
    const int numPossibilities = static_cast< int >( possibilities.size() );
    int randomIndex = mLargeDistribution( mRandom ) % numPossibilities;
    // Recursive call. Note: Improvement would be to mark when a particular
    // branch has been fully explored
    NodePtr randomChild = possibilities[ randomIndex ];
    deepDive( randomChild, !userTurn );
    updateTotals( choiceNode );
    mState = choice->reverse( mState );
  }
  
 private:

  void updateTotals( NodePtr node )
  {
    int newWinsTotal = 0;
    int newTiesTotal = 0;
    int newLossesTotal = 0;
    bool nodeIsFinished = true;
    for ( NodePtr child : node->children() ) {
      nodeIsFinished &= child->isFinished();
      newWinsTotal += child->wins();
      newLossesTotal += child->losses();
      newTiesTotal += child->totalSimsRun() - child->wins() - child->losses();
    }
    if ( nodeIsFinished ) {
      node->setFinished();
    }
    node->setWins( newWinsTotal );
    node->setTies( newTiesTotal );
    node->setLosses( newLossesTotal );
  }
  
  bool isEndState( NodePtr choiceNode, bool userTurn )
  {
    Choice* choice = choiceNode->myMove();
    if ( mUser.hasWon( mState, *choice ) ) {
      choiceNode->setLosses( 1 );
      choiceNode->setFinished();
      mState = choice->reverse( mState );
      return true;
    } else if ( mComputer.hasWon( mState, *choice ) ) {
      choiceNode->setWins( 1 );
      choiceNode->setFinished();
      mState = choice->reverse( mState );
      return true;
    }
    if ( choiceNode->children().empty() ) {
      std::vector< std::unique_ptr< Choice > > newChildren = userTurn ?
        mUser.choices( mState )
        : mComputer.choices( mState );
      if ( newChildren.empty() ) {
        // No more possibilites, hit a draw
        choiceNode->setTies( 1 );
        choiceNode->setFinished();
        mState = choice->reverse( mState );
        return true;
      }
      for ( auto&& newChild : newChildren ) {
        choiceNode->addChild( std::make_shared< Node >( std::move( newChild ), choiceNode ) );
      }
    }
    return false;
  }

  NodePtr chooseChildToSimulate( int total, const std::vector< NodePtr >& children, bool userTurn )
  {
    if ( total == 0 ) {
      int randomIndex = mLargeDistribution( mRandom ) % children.size();
      return children[ randomIndex ];
    }
    constexpr double defaultValue = 2.0;
    constexpr double weight = .5;
    constexpr double fuzz = 1e-6;
    std::vector< NodePtr > bestChildren;
    double bestValue = 0.0;
    std::vector< double > childValues;
    for ( const auto& child : children ) {
      double value = defaultValue;
      int childTotal = child->totalSimsRun();
      double numerator = static_cast< double >( userTurn ? child->losses() : child->wins() );
      if ( childTotal != 0 ) {
        value = numerator / childTotal + weight * std::sqrt( std::log( total ) / childTotal );
      }
      childValues.push_back( value );
      if ( value > bestValue +fuzz ) {
        bestValue = value;
        bestChildren.clear();
        bestChildren.push_back( child );
      } else if ( value + fuzz > bestValue ) {
        bestChildren.push_back( child );
      }
    }
    if ( bestChildren.size() == 1 ) {
      return *bestChildren.begin();
    } else {
      int randomIndex = mLargeDistribution( mRandom ) % bestChildren.size();
      return bestChildren[ randomIndex ];
    }
  }
  
  NodePtr mLastPlayed = nullptr;
  State mState;
  Analyzer mUser;
  Analyzer mComputer;
  Timer mTimer;
  std::mt19937 mRandom;
  std::uniform_int_distribution<std::mt19937::result_type> mLargeDistribution{1, 1000000};

};

#endif
